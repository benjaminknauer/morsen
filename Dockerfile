FROM node:17-alpine

COPY . /morsen/
WORKDIR /morsen

RUN npm ci

EXPOSE 80/tcp
CMD npm start