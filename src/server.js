const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const {Server} = require("socket.io");
const io = new Server(server);

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/start.html');
});

app.get('/send', (req, res) => {
    res.sendFile(__dirname + '/send.html');
});

app.get('/receive', (req, res) => {
    res.sendFile(__dirname + '/receive.html');
});

app.get('/room', (req, res) => {
    res.sendFile(__dirname + '/room.html');
});

app.get('/styles.css', (req, res) => {
    res.sendFile(__dirname + '/styles.css');
});

const receivers = new Map();

io.on('connection', (socket) => {
    const {receiverId} = socket.handshake.query;
    const userId = socket.id;
    if (receiverId) {
        receivers.set(receiverId, socket);
    }

    console.log(`user ${userId} connected. receiverId: ${receiverId}`);


    socket.on("connection-established", ({receiverId}) => {
        const receiver = receivers.get(receiverId);
        if (receiver) {
            receiver.emit("connection-established");
        }
    })

    socket.on('morse', ({state, receiverId}) => {
        const receiver = receivers.get(receiverId);
        if (receiver) {
            receiver.emit("morse-update", state);
        }
    });

    socket.on("disconnecting", () => {
        console.log(`user ${userId} disconnected - receiverId: ${receiverId}`);
        if (receiverId) {
            receivers.delete(receiverId);
        }
    })
});

server.listen(80, () => {
    console.log('listening on *:80');
});